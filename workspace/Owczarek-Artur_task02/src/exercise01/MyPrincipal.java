package exercise01;

import java.security.Principal;

public class MyPrincipal implements Principal {
	private String key;
	private String value;

	public MyPrincipal(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getName() {
		return key + "=" + value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null) {
			return false;
		}
		if (!(o instanceof MyPrincipal)) {
			return false;
		}
		MyPrincipal user2 = (MyPrincipal) o;
		return getName().equals(user2.getName());
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
