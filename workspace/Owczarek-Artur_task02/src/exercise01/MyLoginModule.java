package exercise01;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

public class MyLoginModule implements LoginModule {
	private static Logger logger = Logger.getLogger(MyLoginModule.class
			.getName());
	private Subject subject;
	private CallbackHandler callbackHandler;
	private Map<String, ?> options;

	@Override
	public boolean abort() throws LoginException {
		return true;
	}

	@Override
	public boolean commit() throws LoginException {
		return true;
	}

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler,
			Map<String, ?> sharedState, Map<String, ?> options) {
		logger.info("MyLoginModule initialization");

		this.subject = subject;
		this.callbackHandler = callbackHandler;
		this.options = options;
	}

	@Override
	public boolean login() throws LoginException {
		logger.info("Trying to log in using MyLoginModule");
		if (callbackHandler == null) {
			throw new LoginException("There is no handler");
		}

		NameCallback nameCallback = new NameCallback("Login: ");
		PasswordCallback passwordCallback = new PasswordCallback("Password: ",
				true);
		Callback[] callbacks = new Callback[] { nameCallback, passwordCallback };

		try {
			callbackHandler.handle(callbacks);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedCallbackException e) {
			e.printStackTrace();
		}

		String userName = nameCallback.getName();
		String password = new String(passwordCallback.getPassword());

		boolean trialSucceeded = tryLogIn(userName, password);
		return trialSucceeded;
	}

	@Override
	public boolean logout() throws LoginException {
		return true;
	}

	private boolean tryLogIn(String userName, String password) {
		String fileWithUsers = "users.secret";
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(fileWithUsers));
			while (scanner.hasNextLine()) {
				String userDataLine = scanner.nextLine();
				String[] userPasswordRole = userDataLine.split("\\|");

				boolean userNameMatches = userPasswordRole[0].equals(userName);
				boolean passwordMatches = userPasswordRole[1].equals(password);

				if (userNameMatches && passwordMatches) {
					Set<Principal> principals = subject.getPrincipals();
					principals.add(new MyPrincipal("user", userName));
					String role = userPasswordRole[2];
					principals.add(new MyPrincipal("role", role));
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("File " + fileWithUsers
					+ " with users data has not been found");
			return false;
		} finally {
			scanner.close();
		}
		return false;
	}

}
