package exercise01;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.swing.*;

public class PropertiesAndFileAccessApplet extends JApplet {
	private static Logger logger = Logger
			.getLogger(PropertiesAndFileAccessApplet.class.getName());
	private JTextArea taContainer;
	private JTextField tfUser;
	private JPasswordField pfPassword;
	private JButton bLogIn;
	private JLabel llogInResult;

	@Override
	public void init() {
		logger.info("Applet initialization");
		createComponents();
		deployComponents();
		addListeners();
	}

	@Override
	public void start() {
		logger.info("Starting applet");
	}

	private void createComponents() {
		logger.info("Creating components");
		taContainer = new JTextArea();
		tfUser = new JTextField();
		pfPassword = new JPasswordField();
		bLogIn = new JButton("Log in");
		llogInResult = new JLabel("Not logged in yet");
	}

	private void deployComponents() {
		logger.info("Deploying components");
		setLayout(new BorderLayout());

		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(3, 2));
		topPanel.add(new JLabel("User:"));
		topPanel.add(tfUser);
		topPanel.add(new JLabel("Password:"));
		topPanel.add(pfPassword);
		topPanel.add(bLogIn);
		topPanel.add(llogInResult);

		JScrollPane scrollPane = new JScrollPane(taContainer);

		add(topPanel, BorderLayout.PAGE_START);
		add(scrollPane, BorderLayout.CENTER);
	}

	private void addListeners() {
		logger.info("Adding listeners");
		bLogIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logIn();
			}
		});
	}

	private void listProperties() {
		Properties properties = System.getProperties();

		for (Map.Entry<Object, Object> property : properties.entrySet()) {
			taContainer.append(property.toString());
			taContainer.append("\n");
		}
	}

	private void listFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner skaner = new Scanner(file);
			while (skaner.hasNextLine()) {
				String line = skaner.nextLine();
				taContainer.append(line);
				taContainer.append("\n");
			}
			skaner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void logIn() {
		String userName = tfUser.getText();
		String password = new String(pfPassword.getPassword());

		logger.info("Trying to log in using user name " + userName
				+ " and password " + password);

		try {
			LoginContext context = new LoginContext("MyLoginContext",
					new MyCallbackHandler(userName, password));
			context.login();

			Subject subject = context.getSubject();

			Subject.doAsPrivileged(subject, new PrivilegedAction<String>() {
				@Override
				public String run() {
					try {
						listProperties();
					} catch (SecurityException e) {
						taContainer
								.append("Brak uprawnie� do odczytu/zapisu w�a�ciwo�ci systemowych!\n");
						taContainer.append(e.getMessage());
						taContainer.append("\n");
					}
					try {
						listFile("C:\\Windows\\system32\\drivers\\etc\\hosts");
					} catch (SecurityException e) {
						taContainer
								.append("Brak uprawnie� do odczytu pliku C:\\Windows\\system32\\drivers\\etc\\hosts\n");
						taContainer.append(e.getMessage());
						taContainer.append("\n");
					}
					return null;
				}
			}, null);

			context.logout();
		} catch (LoginException e) {
			logger.info(e.getMessage());
			taContainer.append("B��d logowania\n");
		}
	}
}
