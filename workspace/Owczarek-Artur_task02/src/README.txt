Aplikacj� nale�y uruchomi� podaj�c na wej�ciu plik konfiguracyjny JAAS:
-Djava.security.auth.login.config=jaas.conf
Dodatkowo trzeba poda� plik z polityk� bezpiecze�stwa. Nale�y si� upewni�, �e plik ten pozwala na dost�p do pliku users.secret
-Djava.security.policy==src/exercise01/java.policy.applet -Djava.security.manager
Je�li applet zostanie uruchomiony z poziomu �rodowiska Eclipse, to wczytanie pliku polityki bezpiecze�stwa java.policy.applet odb�dzie si� automatycznie. Trzeba tylko zadba� o plik z konfiguracj� JAAS.

Plik users.secret zawiera list� u�ytkownik�w przypisanych do r�nych r�l.
U�ytkownicy w roli admin zobacz� plik hosts i wy�wietl� im si� propertiesy
U�ytkownicy normal zobacz� tylko propertiesy
U�ytkownicy z innymi rolami nic nie mog� zrobi�
Je�li nie uda si� zalogowa�, to u�ytkownik zobaczy odpowiedni komunikat.