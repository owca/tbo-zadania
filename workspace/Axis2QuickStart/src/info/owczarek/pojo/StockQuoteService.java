package info.owczarek.pojo;

import java.util.HashMap;

public class StockQuoteService {
	private HashMap<String, Double> quotes = new HashMap<>();
	
	public double getPrice(String symbol) {
		Double price = quotes.get(symbol);
		return price;
	}
	
	public void update(String symbol, double price) {
		quotes.put(symbol, price);
	}
}
