package exercise01;

import java.security.Security;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Main {

	public static void main(String[] args) {
		Security.insertProviderAt(new BouncyCastleProvider(), 0);
		
		CommandLine commandLine =  getCommandLine(args);
		if (commandLine == null) {
			return;
		}
		
		Controller controller = new Controller();
		controller.execute(commandLine);
	}
	
	private static Options prepareCommandLineOptions() {
		Options commandLineOptions = new Options();
		commandLineOptions.addOption("outputFile", true, "Plik do kt�rego zostanie zapisany rezultat");
		commandLineOptions.addOption("inputFile", true, "Plik wej�ciowy");
		
		commandLineOptions.addOption("digest", false, "Wylicza skr�t dla pliku wej�ciowego");
		commandLineOptions.addOption("algorithm", true, "Algorytm");
		
		commandLineOptions.addOption("text", true, "Tekst kt�rego skr�t obliczy�");
		
		commandLineOptions.addOption("gensymkey", false, "Generuje klucz symetryczny");
		commandLineOptions.addOption("symkeyalg", true, "Algorytm wykorzystywany podczas generowania klucza symetrycznego (np AES, DES)");
		
		commandLineOptions.addOption("keystore", true, "Miejsce przechowywania kluczy");
		commandLineOptions.addOption("alias", true, "Alias");
		commandLineOptions.addOption("storepass", true, "Has�o do keystore");
		
		
		commandLineOptions.addOption("encrypt", false, "Zakoduj plik");
		commandLineOptions.addOption("decrypt", false, "Zdekoduj plik");
		commandLineOptions.addOption("keyFile", true, "Plik z kluczem");
		
		commandLineOptions.addOption("genkeypair", false, "Generuje par� kluczy");
		commandLineOptions.addOption("publicKeyFile", true, "�cie�ka gdzie zapisa� klucz publiczny");
		commandLineOptions.addOption("privateKeyFile", true, "�cie�ka gdzie zapisa� klucz prywatny");
		
		commandLineOptions.addOption("list", false, "listuje zawarto�� keystore");
		commandLineOptions.addOption("keyStorePassword", true, "Has�o do keystore");
		commandLineOptions.addOption("keypass", true, "");
		commandLineOptions.addOption("genCsr", false, "Generuje CSR");
		commandLineOptions.addOption("signCsr", false, "Generuje CSR");
		
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");
//		commandLineOptions.addOption("", true, "");

		return commandLineOptions;
	}

	private static CommandLine getCommandLine(String[] args) {
		CommandLineParser parser = new GnuParser();
		Options commandLineOptions = prepareCommandLineOptions();
		try {
			return parser.parse(commandLineOptions, args);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
