package exercise01;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.bouncycastle.operator.OperatorCreationException;

public class Controller {
	private KeyTool keyTool = new KeyTool();

	public void execute(CommandLine commandLine) {
		if (commandLine.hasOption("digest")) {
			digest(commandLine);
		} else if (commandLine.hasOption("gensymkey")) {
			generateSymmetricKeyAndSaveToFile(commandLine);
		} else if (commandLine.hasOption("encrypt")) {
			encryptFile(commandLine);
		} else if (commandLine.hasOption("decrypt")) {
			decryptFile(commandLine);
		} else if (commandLine.hasOption("genkeypair")) {
			generateKeyPair(commandLine);
		} else if (commandLine.hasOption("list")) {
			listKeyStore(commandLine);
		} else if (commandLine.hasOption("genCsr")) {
			genCsr(commandLine);
		} else if (commandLine.hasOption("signCsr")) {
			signCsr(commandLine);
		} else if (commandLine.hasOption("loadCert")) {
			loadCert(commandLine);
		} else if (commandLine.hasOption("signFile")) {
			signFile(commandLine);
		} else if (commandLine.hasOption("genMac")) {
			signFile(commandLine);
		}
		
	}

	private void digest(CommandLine commandLine) {
		String algorithm = commandLine.getOptionValue("algorithm");
		String inputFile = commandLine.getOptionValue("inputFile");
		String text = commandLine.getOptionValue("text");

		boolean digestFile = inputFile != null;
		boolean digestText = text != null;

		if (digestFile && digestText) {
			throw new RuntimeException(
					"Nie mo�na na raz oblicza� skr�tu dla pliku i tekstu");
		}

		byte[] digest = null;
		if (digestFile) {
			digest = keyTool.digestFile(inputFile, algorithm);
		} else if (digestText) {
			digest = keyTool.digestText(text, algorithm);
		}
		if (digest != null) {
			String digestAsHexString = keyTool
					.convertByteArrayToHexString(digest);

			System.out.println(algorithm + ": " + digestAsHexString);
		}
	}

	private void generateSymmetricKeyAndSaveToFile(CommandLine commandLine) {
		String algorithm = commandLine.getOptionValue("algorithm");
		
		String outputFile = commandLine.getOptionValue("outputFile");
		
		String keyStore = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String keypass = commandLine.getOptionValue("keypass");
		String alias = commandLine.getOptionValue("alias");

		if (outputFile != null) {
			try {
			keyTool.generateSymmetricKeyAndSaveToFile(algorithm, outputFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (keyStore != null) {
			try {
				keyTool.generateSymmetricKeyAndSaveToKeyStore(algorithm, alias, keypass, keyStore, keyStorePassword);
			} catch (KeyStoreException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void encryptFile(CommandLine commandLine) {
		String algorithm = commandLine.getOptionValue("algorithm");
		String keyFile = commandLine.getOptionValue("keyFile");
		String outputFilePath = commandLine.getOptionValue("outputFile");
		String inputFilePath = commandLine.getOptionValue("inputFile");
		
		try {
			keyTool.encrypt(keyFile, algorithm, inputFilePath, outputFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void decryptFile(CommandLine commandLine) {
		String algorithm = commandLine.getOptionValue("algorithm");
		String keyFile = commandLine.getOptionValue("keyFile");
		String outputFilePath = commandLine.getOptionValue("outputFile");
		String inputFilePath = commandLine.getOptionValue("inputFile");
		
		try {
			keyTool.decrypt(keyFile, algorithm, inputFilePath, outputFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void generateKeyPair(CommandLine commandLine) {
		String algorithm = commandLine.getOptionValue("algorithm");
		String privateKeyPath = commandLine.getOptionValue("privateKeyFile");
		String publicKeyPath = commandLine.getOptionValue("publicKeyFile");
		
		String keyStoreFilePath = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String keypass = commandLine.getOptionValue("keypass");
		String alias = commandLine.getOptionValue("alias");
		
		if (privateKeyPath != null) {
			try {
				keyTool.generateKeyPairAndSaveToFiles(algorithm, privateKeyPath, publicKeyPath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (keyStoreFilePath != null) {
			try {
				keyTool.generateKeyPairAndAddToKeyStore(alias, algorithm, keyStoreFilePath, keyStorePassword, keypass);
			} catch (KeyStoreException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (NoSuchProviderException e) {
				e.printStackTrace();
			} catch (SignatureException e) {
				e.printStackTrace();
			} catch (OperatorCreationException e) {
				e.printStackTrace();
			}
		}
	}

	private void listKeyStore(CommandLine commandLine) {
		String keyStorePath = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		try {
			keyTool.listKeyStore(keyStorePath, keyStorePassword);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
	}
	
	private void genCsr(CommandLine commandLine) {
		String keyStoreLocation = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String alias = commandLine.getOptionValue("alias");
		String keyPassword = commandLine.getOptionValue("keypass");
		String csrFilePath = commandLine.getOptionValue("outputFile");
		keyTool.generateCsrAndSaveToFile(keyStoreLocation, keyStorePassword, alias, keyPassword, csrFilePath);
	}
	
	private void signCsr(CommandLine commandLine) {
		String keyStoreLocation = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String alias = commandLine.getOptionValue("alias");
		String keyPassword = commandLine.getOptionValue("keypass");
		String replyFilePath = commandLine.getOptionValue("outputFile");
		String csrFilePath = commandLine.getOptionValue("inputFile");
		keyTool.signCsr(alias, keyPassword, keyStoreLocation, keyStorePassword, csrFilePath, replyFilePath);
	}
	
	private void loadCert(CommandLine commandLine) {
		String keyStoreLocation = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String alias = commandLine.getOptionValue("alias");
		String certFile = commandLine.getOptionValue("inputFile");
		keyTool.loadCertificatesToKeyStore(keyStoreLocation, keyStorePassword, certFile, alias);
	}
	
	private void signFile(CommandLine commandLine) {
		String keyStoreLocation = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String alias = commandLine.getOptionValue("alias");
		String keyPassword = commandLine.getOptionValue("keypass");
		String outputFilePath = commandLine.getOptionValue("outputFile");
		String inputFilePath = commandLine.getOptionValue("inputFile");
		keyTool.signFile(keyStoreLocation, keyStorePassword, alias, keyPassword, inputFilePath, outputFilePath);
	}
	
	private void genMac(CommandLine commandLine) {
		String keyStoreLocation = commandLine.getOptionValue("keystore");
		String keyStorePassword = commandLine.getOptionValue("keyStorePassword");
		String alias = commandLine.getOptionValue("alias");
		String keyPassword = commandLine.getOptionValue("keypass");
		String outputFilePath = commandLine.getOptionValue("outputFile");
		String inputFilePath = commandLine.getOptionValue("inputFile");
		keyTool.genMac(keyStoreLocation, keyStorePassword, alias, keyPassword, inputFilePath, outputFilePath);
	}
}
