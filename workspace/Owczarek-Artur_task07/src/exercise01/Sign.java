package exercise01;

import java.security.KeyPair;
import java.util.Scanner;

public class Sign {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj �cie�k� do pliku xml");
		String input = scanner.nextLine();
		System.out.println("Podaj �cie�k� gdzie ma by� zapisany podpisany plik");
		String output = scanner.nextLine();
		System.out.println("Gdzie zapisa� obiekt z kluczem publicznym?");
		String publicKeyFile = scanner.nextLine();
		KeyPair keyPair = Library.getKeyPair();
		Library.signDocument(input, output, keyPair);
		Library.savePublicKeyObjectToFile(publicKeyFile, keyPair.getPublic());
		System.out.println("Done");
		scanner.close();
	}

}
