package exercise01;

import java.security.PublicKey;
import java.util.Scanner;

public class Verify {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj �cie�k� do podpisanego pliku xml");
		String documentPath = scanner.nextLine();
		System.out.println("Podaj �cie�k� do zserializowanego klucza publicznego");
		String publicKeyPath = scanner.nextLine();
		PublicKey publicKey = Library.readPublicKeyObject(publicKeyPath);
		boolean isValid = Library.isDocumentValid(documentPath, publicKey);
		System.out.println("Document " + (isValid ? "is" : "isn't") + " valid");
		scanner.close();
	}
}
