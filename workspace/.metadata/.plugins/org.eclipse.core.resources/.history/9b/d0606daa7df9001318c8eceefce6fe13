package exercise01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.PublicKey;
import java.util.Collections;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Library {
	public static void signDocument(String fileToSign, String signedFile, KeyPair keyPair) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyException, FileNotFoundException, SAXException,
			IOException, ParserConfigurationException, MarshalException, XMLSignatureException, TransformerException {
		String providerName = System.getProperty("jsr105Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");

		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

		Reference reference = signatureFactory.newReference("", signatureFactory.newDigestMethod(DigestMethod.SHA1, null),
				Collections.singletonList(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);

		SignedInfo signedInfo = signatureFactory.newSignedInfo(
				signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS, (C14NMethodParameterSpec) null),
				signatureFactory.newSignatureMethod(SignatureMethod.DSA_SHA1, null), Collections.singletonList(reference));

		KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
		KeyValue keyValue = keyInfoFactory.newKeyValue(keyPair.getPublic());

		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(keyValue));

		Document doc = getDocument(fileToSign);

		DOMSignContext dsc = new DOMSignContext(keyPair.getPrivate(), doc.getDocumentElement());

		XMLSignature signature = signatureFactory.newXMLSignature(signedInfo, keyInfo);
		signature.sign(dsc);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(signedFile)));
	}

	public static boolean isDocumentValid(String documentPath, PublicKey publicKey) throws Exception {
		Document document = getDocument(documentPath);
		NodeList nodes = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if (nodes.getLength() == 0) {
			throw new Exception("W dokumencie nie ma podpisu");
		}

		DOMValidateContext validateContext = new DOMValidateContext(publicKey, nodes.item(0));
		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
		XMLSignature signature = signatureFactory.unmarshalXMLSignature(validateContext);
		boolean validFlag = signature.validate(validateContext);
		return validFlag;
	}

	public static Document getDocument(String documentPath) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		Document doc = documentBuilderFactory.newDocumentBuilder().parse(new FileInputStream(documentPath));
		return doc;
	}

	public static KeyPair getKeyPair() {
		KeyPair keyPair = null;
		try {
			KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DSA");
			keyGenerator.initialize(512);
			keyPair = keyGenerator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return keyPair;
	}
	
	public static void savePublicKeyObjectToFile(String publicKeyFile, PublicKey publicKey) {
		try {
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(publicKeyFile));
			outputStream.writeObject(publicKey);
			outputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
