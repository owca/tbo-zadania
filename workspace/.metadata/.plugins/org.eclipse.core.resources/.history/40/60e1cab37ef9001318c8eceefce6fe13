package exercise01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.util.*;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.*;
import javax.xml.crypto.dsig.keyinfo.*;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class Main {
	public static void main(String[] args) {
		try {
			KeyPair keyPair = getKeyPair();
			String input = "books.xml";
			String output = "books_signed.xml";
			signDocument(input, output, keyPair);
			System.out.println(isDocumentValid(output, keyPair.getPublic()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void signDocument(String fileToSign, String signedFile, KeyPair keyPair) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyException, FileNotFoundException, SAXException,
			IOException, ParserConfigurationException, MarshalException, XMLSignatureException, TransformerException {
		String providerName = System.getProperty("jsr105Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");

		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

		Reference reference = signatureFactory.newReference("", signatureFactory.newDigestMethod(DigestMethod.SHA1, null),
				Collections.singletonList(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);

		SignedInfo signedInfo = signatureFactory.newSignedInfo(
				signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS, (C14NMethodParameterSpec) null),
				signatureFactory.newSignatureMethod(SignatureMethod.DSA_SHA1, null), Collections.singletonList(reference));

		KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
		KeyValue keyValue = keyInfoFactory.newKeyValue(keyPair.getPublic());

		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(keyValue));

		Document doc = getDocument(fileToSign);

		DOMSignContext dsc = new DOMSignContext(keyPair.getPrivate(), doc.getDocumentElement());

		XMLSignature signature = signatureFactory.newXMLSignature(signedInfo, keyInfo);
		signature.sign(dsc);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(signedFile)));
	}

	public static boolean isDocumentValid(String documentPath, PublicKey publicKey) throws Exception {
		Document document = getDocument(documentPath);
		NodeList nodes = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if (nodes.getLength() == 0) {
			throw new Exception("W dokumencie nie ma podpisu");
		}

		DOMValidateContext validateContext = new DOMValidateContext(publicKey, nodes.item(0));
		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
		XMLSignature signature = signatureFactory.unmarshalXMLSignature(validateContext);
		boolean validFlag = signature.validate(validateContext);
		return validFlag;
	}

	private static Document getDocument(String documentPath) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		Document doc = documentBuilderFactory.newDocumentBuilder().parse(new FileInputStream(documentPath));
		return doc;
	}

	private static KeyPair getKeyPair() throws NoSuchAlgorithmException {
		KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DSA");
		keyGenerator.initialize(512);
		KeyPair keyPair = keyGenerator.generateKeyPair();
		return keyPair;
	}
}
