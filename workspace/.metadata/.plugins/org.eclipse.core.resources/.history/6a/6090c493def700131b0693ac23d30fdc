package exercise01;

import static org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers.pkcs_9_at_extensionRequest;
import static org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers.pkcs_9_at_unstructuredName;

import java.io.*;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.cert.X509Extension;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V1CertificateGenerator;

public class KeyTool {
	private static final Logger LOGGER = Logger.getLogger(KeyTool.class.getName());
	private static final String DEFAULT_KEY_STORE_TYPE = "jceks";
	private static final int DEFAULT_KEY_SIZE = 1024;

	public KeyStore loadKeyStore(String keyStoreLocation, char[] password, String keyStoreType) {
		KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance(keyStoreType);
		} catch (KeyStoreException e1) {
			e1.printStackTrace();
		}
		InputStream inputStream = null;
		try {
			if (keyStoreLocation != null) {
				inputStream = new FileInputStream(keyStoreLocation);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			keyStore.load(inputStream, password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return keyStore;
	}

	public KeyStore loadKeyStore(String keyStoreLocation, char[] password) {
		return loadKeyStore(keyStoreLocation, password, DEFAULT_KEY_STORE_TYPE);
	}

	public byte[] digestFile(String inputFilePath, String algorithm) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		InputStream inputFileStream = null;
		try {
			inputFileStream = new FileInputStream(inputFilePath);

			byte[] buffer = new byte[1024];

			int numberOfBytesRead = 0;
			while ((numberOfBytesRead = inputFileStream.read(buffer)) != -1) {
				messageDigest.update(buffer, 0, numberOfBytesRead);
			}
			inputFileStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputFileStream != null) {
				try {
					inputFileStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		byte[] digest = messageDigest.digest();

		return digest;
	}

	public byte[] digestText(String textToDigest, String algorithm) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		messageDigest.update(textToDigest.getBytes());

		byte[] digest = messageDigest.digest();

		return digest;
	}

	public String convertByteArrayToHexString(byte[] byteArray) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < byteArray.length; i++) {
			stringBuilder.append(Integer.toString((byteArray[i] & 0xff) + 0x100, 16).substring(1));
		}

		return stringBuilder.toString();
	}

	public Key generateSymmetricKey(String algorithm) {
		LOGGER.info("Generating symmetric key using algorithm " + algorithm);
		KeyGenerator keyGenerator = null;
		try {
			keyGenerator = KeyGenerator.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		SecureRandom random = new SecureRandom();
		keyGenerator.init(random);

		Key key = keyGenerator.generateKey();
		return key;
	}

	public KeyPair generateKeyPair(String algorithm) {
		return generateKeyPair(algorithm, 1024);
	}

	public KeyPair generateKeyPair(String algorithm, int keySize) {
		LOGGER.info("Generating symmetric key par using algorithm " + algorithm + " with key size " + keySize);
		KeyPairGenerator keyPairGenerator = null;
		try {
			keyPairGenerator = KeyPairGenerator.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		SecureRandom random = new SecureRandom();
		keyPairGenerator.initialize(keySize, random);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		return keyPair;
	}

	public void generateSymmetricKeyAndSaveToFile(String algorithm, String filePath) throws FileNotFoundException, IOException {
		Key key = generateSymmetricKey(algorithm);
		saveKeyToFile(key, filePath);
	}

	public void encrypt(String keyFile, String algorithm, String inputFilePath, String outputFilePath) throws Exception {
		LOGGER.info("Encrypting file " + inputFilePath + " into " + outputFilePath + " with " + algorithm);
		Key key = readKeyFromFile(keyFile, algorithm);
		System.out.println(convertByteArrayToHexString(key.getEncoded()));
		InputStream inputStream = new FileInputStream(inputFilePath);
		OutputStream outputStream = new FileOutputStream(outputFilePath);
		encrypt(key, algorithm, inputStream, outputStream);
		inputStream.close();
		outputStream.flush();
		outputStream.close();
	}

	public void encrypt(Key key, String algorithm, InputStream inputStream, OutputStream outputStream) throws Exception {
		crypt(key, algorithm, inputStream, outputStream, Cipher.ENCRYPT_MODE);
	}

	public void decrypt(String keyFile, String algorithm, String inputFilePath, String outputFilePath) throws Exception {
		LOGGER.info("Decrypting file " + inputFilePath + " into " + outputFilePath + " with " + algorithm);
		Key key = readKeyFromFile(keyFile, algorithm);
		System.out.println(convertByteArrayToHexString(key.getEncoded()));
		InputStream inputStream = new FileInputStream(inputFilePath);
		OutputStream outputStream = new FileOutputStream(outputFilePath);
		decrypt(key, algorithm, inputStream, outputStream);
		inputStream.close();
		outputStream.flush();
		outputStream.close();
	}

	public void decrypt(Key key, String algorithm, InputStream inputStream, OutputStream outputStream) throws Exception {
		crypt(key, algorithm, inputStream, outputStream, Cipher.DECRYPT_MODE);
	}

	public void crypt(Key key, String algorithm, InputStream inputStream, OutputStream outputStream, int mode) throws Exception {
		Cipher cipher = getCipher(key, algorithm, mode);
		cryptStreams2(cipher, inputStream, outputStream);
	}

	public Cipher getCipher(Key key, String algorithm, int mode) {
		LOGGER.info("Generating cipher for key using algorithm " + algorithm);
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(mode, key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		return cipher;
	}

	@Deprecated
	public void cryptStreams1(Cipher cipher, InputStream inputStream, OutputStream outputStream) throws Exception {
		int blockSize = cipher.getBlockSize();
		int outputSize = cipher.getOutputSize(blockSize);
		LOGGER.info("Cipher has block size=" + blockSize + " and outputSize=" + outputSize);

		byte[] inputBytes = new byte[blockSize];
		byte[] outputBytes = new byte[outputSize];

		int numberOfBytesRead = 0;
		boolean areMoreBytesToRead = true;
		while (areMoreBytesToRead) {
			numberOfBytesRead = inputStream.read(inputBytes);
			boolean wholeBlockHasBeenRead = numberOfBytesRead == blockSize;
			if (wholeBlockHasBeenRead) {
				int outputLength = cipher.update(inputBytes, 0, blockSize, outputBytes);
				outputStream.write(outputBytes, 0, outputLength);
			} else {
				areMoreBytesToRead = false;
			}
		}

		if (numberOfBytesRead > 0) {
			cipher.doFinal(inputBytes, 0, numberOfBytesRead, outputBytes);
		} else {
			cipher.doFinal();
		}

		outputStream.write(outputBytes);
	}

	public void cryptStreams2(Cipher cipher, InputStream inputStream, OutputStream outputStream) throws IOException {
		CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, cipher);
		int bytesRead = 0;

		int blockSize = cipher.getBlockSize();
		byte[] inputBytes = new byte[blockSize];

		int numberOfBytesRead = 0;
		boolean areMoreBytesToRead = true;
		while (areMoreBytesToRead) {
			numberOfBytesRead = inputStream.read(inputBytes);
			bytesRead += numberOfBytesRead;
			if (numberOfBytesRead > 0) {
				cipherOutputStream.write(inputBytes);
			} else {
				areMoreBytesToRead = false;
			}
		}

		LOGGER.info("Number of bytes read: " + bytesRead);
		cipherOutputStream.flush();
		cipherOutputStream.close();
	}

	public Key readKeyFromFile(String keyFile, String algorithm) throws ClassNotFoundException, IOException, NoSuchAlgorithmException {
		FileInputStream keyInputStream = new FileInputStream(keyFile);
		byte[] keyBytes = new byte[keyInputStream.available()];
		keyInputStream.read(keyBytes);
		keyInputStream.close();

		Key key = new SecretKeySpec(keyBytes, algorithm);

		return key;
	}

	public void generateKeyPairAndSaveToFiles(String algorithm, String privateKeyPath, String publicKeyPath) throws FileNotFoundException,
			IOException {
		KeyPair keyPair = generateKeyPair(algorithm);
		saveKeyPairToFiles(keyPair, privateKeyPath, publicKeyPath);
	}

	private void saveKeyPairToFiles(KeyPair keyPair, String privateKeyPath, String publicKeyPath) throws FileNotFoundException, IOException {
		Key publicKey = keyPair.getPublic();
		Key privateKey = keyPair.getPrivate();
		saveKeyToFile(privateKey, privateKeyPath);
		saveKeyToFile(publicKey, publicKeyPath);
	}

	private void saveKeyToFile(Key key, String keyPath) throws FileNotFoundException, IOException {
		byte[] keyBytes = key.getEncoded();
		OutputStream keyOutputStream = new FileOutputStream(keyPath);
		keyOutputStream.write(keyBytes);
		keyOutputStream.flush();
		keyOutputStream.close();
	}

	public KeyStore getNewKeyStore() {
		return getNewKeyStore("jceks");
	}

	public KeyStore getNewKeyStore(String type) {
		try {
			KeyStore keyStore = KeyStore.getInstance(type);
			keyStore.load(null, null);
			return keyStore;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public KeyStore getKeyStore(String keyStorePath, String password) {
		return getKeyStore("jceks", keyStorePath, password);
	}

	public KeyStore getKeyStore(String type, String keyStorePath, String password) {
		try {
			KeyStore keyStore = KeyStore.getInstance(type);
			InputStream keyStoreInputStream = new FileInputStream(keyStorePath);
			keyStore.load(keyStoreInputStream, password.toCharArray());
			return keyStore;
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void generateKeyPairAndAddToKeyStore(String alias, String algorithm, String keyStoreFilePath, String keyStorePassword, String keyPassword)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, InvalidKeyException, IllegalStateException,
			NoSuchProviderException, SignatureException, OperatorCreationException {
		generateKeyPairAndAddToKeyStore(alias, algorithm, DEFAULT_KEY_SIZE, keyStoreFilePath, keyStorePassword, keyPassword);
	}

	public void generateKeyPairAndAddToKeyStore(String alias, String algorithm, int keySize, String keyStoreFilePath, String keyStorePassword,
			String keyPassword) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, InvalidKeyException,
			IllegalStateException, NoSuchProviderException, SignatureException, OperatorCreationException {
		KeyPair keyPair = generateKeyPair(algorithm, keySize);
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();
		KeyStore keyStore = loadKeyStore(keyStoreFilePath, keyStorePassword.toCharArray());
		X509Certificate certificate = generateX509Version1(keyPair, "SHA512withRSA");
		keyStore.setKeyEntry(alias, publicKey, keyPassword.toCharArray(), new X509Certificate[] { certificate });
		keyStore.setKeyEntry(alias, privateKey, keyPassword.toCharArray(), new X509Certificate[] { certificate });
		saveKeyStore(keyStore, keyStoreFilePath, keyStorePassword.toCharArray());
	}

	// public X509Certificate getCertificate(KeyPair keyPair) throws
	// InvalidKeyException, IllegalStateException, NoSuchProviderException,
	// NoSuchAlgorithmException, SignatureException, OperatorCreationException,
	// CertificateException {
	//
	// X500NameBuilder builder = createX500NameBuilder();
	// ContentSigner sigGen = new
	// JcaContentSignerBuilder("GOST3411withGOST3410").build(keyPair.getPrivate());
	// X509v3CertificateBuilder certGen = new
	// JcaX509v3CertificateBuilder(builder.build(), BigInteger.valueOf(1), new
	// Date(
	// System.currentTimeMillis() - 50000), new Date(System.currentTimeMillis()
	// + 50000), builder.build(), keyPair.getPublic());
	//
	// X509Certificate certificate = new
	// JcaX509CertificateConverter().getCertificate(certGen.build(sigGen));
	// return certificate;
	// }

	public X509Certificate generateX509Version1(KeyPair keyPair, String signatureAlgorithm) throws CertificateEncodingException, InvalidKeyException,
			IllegalStateException, NoSuchProviderException, NoSuchAlgorithmException, SignatureException {
		Date startDate = new Date(); // time from which certificate is valid
		long oneYearMillis = 365 * 24 * 60 * 60 * 1000;
		Date expiryDate = new Date(System.currentTimeMillis() + oneYearMillis);
		BigInteger serialNumber = new BigInteger(32, new Random());
		X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
		X500Principal dnName = new X500Principal("CN=Test CA Certificate");
		certGen.setSerialNumber(serialNumber);
		certGen.setIssuerDN(dnName);
		certGen.setNotBefore(startDate);
		certGen.setNotAfter(expiryDate);
		certGen.setSubjectDN(dnName); // note: same as issuer
		certGen.setPublicKey(keyPair.getPublic());
		certGen.setSignatureAlgorithm(signatureAlgorithm);
		X509Certificate cert = certGen.generate(keyPair.getPrivate(), "BC");
		return cert;
	}

	public X509Certificate generateX509Version3(X500Name subject, X500Name issuer, long validity, PublicKey publicKey, PrivateKey privateKey) {
		try {
			BigInteger serialNumber = new BigInteger(32, new Random());
			Date notBefore = new Date();
			Date notAfter = new Date(System.currentTimeMillis() + validity);

			JcaX509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(issuer, serialNumber, notBefore, notAfter, subject, publicKey);

			ContentSigner certSigner = new JcaContentSignerBuilder("SHA512withRSA").setProvider("BC").build(privateKey);
			return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(certSigner));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private ASN1Encodable getExtensionValue(X509Extension extensions, String oid) {
		ASN1InputStream ais = null;

		try {
			ais = new ASN1InputStream(extensions.getExtensionValue(oid));
			ASN1Primitive result = ais.readObject();
			ais.close();
			return result;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	// private X500NameBuilder createX500NameBuilder() {
	// X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
	//
	// builder.addRDN(BCStyle.C, "AU");
	// builder.addRDN(BCStyle.O, "The Legion of the Bouncy Castle");
	// builder.addRDN(BCStyle.L, "Melbourne");
	// builder.addRDN(BCStyle.ST, "Victoria");
	// builder.addRDN(BCStyle.E, "feedback-crypto@bouncycastle.org");
	//
	// return builder;
	// }

	public void generateSymmetricKeyAndSaveToKeyStore(String algorithm, String alias, String keypass, String keyStorePath, String keyStorePassword)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		Key key = generateSymmetricKey(algorithm);
		KeyStore keyStore = loadKeyStore(keyStorePath, keyStorePassword.toCharArray());
		keyStore.setKeyEntry(alias, key, keypass.toCharArray(), null);
		saveKeyStore(keyStore, keyStorePath, keyStorePassword.toCharArray());
	}

	// Zapisuje keystore do pliku
	private void saveKeyStore(KeyStore keyStore, String keyStorePath, char[] password) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException {
		OutputStream keyStoreOutputStream = new FileOutputStream(keyStorePath);
		keyStore.store(keyStoreOutputStream, password);
		keyStoreOutputStream.close();
	}

	// Wypisuje wszystko co znajduje si� w keystore
	public void listKeyStore(String keyStorePath, String keyStorePassword) throws KeyStoreException {
		KeyStore keyStore = loadKeyStore(keyStorePath, keyStorePassword.toCharArray());
		listKeyStore(keyStore);
	}

	// Wypisuje wszystkie obiekty w keystore
	public void listKeyStore(KeyStore keyStore) throws KeyStoreException {
		for (Enumeration<String> e = keyStore.aliases(); e.hasMoreElements();) {
			String alias = e.nextElement();
			printKeyStoreEntry(alias, keyStore);
		}
	}

	// Wypisuje czym jest obiekt o podanym aliasie
	public void printKeyStoreEntry(String alias, KeyStore keyStore) throws KeyStoreException {
		if (keyStore.entryInstanceOf(alias, KeyStore.SecretKeyEntry.class)) {
			System.out.println("Klucz " + alias);
		} else if (keyStore.entryInstanceOf(alias, KeyStore.PrivateKeyEntry.class)) {
			System.out.println("Klucz prywatny " + alias);
		} else if (keyStore.entryInstanceOf(alias, KeyStore.TrustedCertificateEntry.class)) {
			System.out.println("Zaufany certyfikat " + alias);
		} else {
			System.out.println("Jaki� inny wynalazek o aliasie " + alias);
		}
	}

	// Generuje csr i zapisuje do pliku
	public void generateCsrAndSaveToFile(String keyStoreLocation, String keyStorePassword, String alias, String keyPassword, String csrFilePath) {
		try {
			KeyStore keyStore = loadKeyStore(keyStoreLocation, keyStorePassword.toCharArray());
			PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, keyPassword.toCharArray());
			X509Certificate firstCertInChain = orderX509CertChain(convertCertificatesToX509(keyStore.getCertificateChain(alias)))[0];
			
			PKCS10CertificationRequest csr = generateCsr(firstCertInChain, privateKey);
			String csrAsString = getCsrEncodedDerPem(csr);
			OutputStream fos = new FileOutputStream(csrFilePath);
			fos.write(csrAsString.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Generuje csr
	public PKCS10CertificationRequest generateCsr(X509Certificate cert, PrivateKey privateKey) {
		JcaPKCS10CertificationRequestBuilder csrBuilder = new JcaPKCS10CertificationRequestBuilder(cert.getSubjectX500Principal(),
				cert.getPublicKey());
		org.bouncycastle.asn1.x509.Certificate certificate = null;
		try {
			certificate = org.bouncycastle.asn1.x509.Certificate.getInstance(cert.getEncoded());
		} catch (CertificateEncodingException e1) {
			e1.printStackTrace();
		}
		Extensions extensions = certificate.getTBSCertificate().getExtensions();
		if (extensions != null) {
			csrBuilder.addAttribute(pkcs_9_at_extensionRequest, extensions.toASN1Primitive());
		}

		PKCS10CertificationRequest csr = null;
		try {
			csr = csrBuilder.build(new JcaContentSignerBuilder("SHA512withRSA").setProvider("BC").build(privateKey));
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		}

		return csr;
	}

	// Zamienia PKCS10CSR na stringa
	public static String getCsrEncodedDerPem(PKCS10CertificationRequest csr) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DEROutputStream deros = new DEROutputStream(baos);
		try {
			deros.writeObject(csr.toASN1Structure().toASN1Primitive());
		} catch (IOException e) {
			e.printStackTrace();
		}
		String tmp = new String(Base64.encode(baos.toByteArray()));

		String csrAsString = "-----BEGIN CERTIFICATE REQUEST-----\n";

		int maxLineLength = 76;
		for (int i = 0; i < tmp.length(); i += maxLineLength) {
			int lineLength;

			if ((i + maxLineLength) > tmp.length()) {
				lineLength = (tmp.length() - i);
			} else {
				lineLength = maxLineLength;
			}

			csrAsString += tmp.substring(i, (i + lineLength)) + "\n";
		}

		csrAsString += "-----END CERTIFICATE REQUEST-----\n";

		return csrAsString;

	}

	// Wczytuje z pliku csr
	public PKCS10CertificationRequest loadCsrFromFile(String filePath) throws IOException {
		byte[] streamContents = readBytesFromFile(filePath);

		byte[] csrBytes = null;
		LineNumberReader lnr = null;

		// Formatem jest prawdopodobnie PEM
		try {
			lnr = new LineNumberReader(new InputStreamReader(new ByteArrayInputStream(streamContents)));

			String line = lnr.readLine();
			StringBuffer sbPem = new StringBuffer();

			if ((line != null) && ((line.equals("-----BEGIN CERTIFICATE REQUEST-----") || line.equals("-----BEGIN NEW CERTIFICATE REQUEST-----")))) {
				while ((line = lnr.readLine()) != null) {
					if (line.equals("-----END CERTIFICATE REQUEST-----") || line.equals("-----END NEW CERTIFICATE REQUEST-----")) {
						csrBytes = Base64.decode(sbPem.toString());
						break;
					}

					sbPem.append(line);
				}
			}
		} finally {
			lnr.close();
		}

		// Je�li nie PEM, to pewnie DER
		if (csrBytes == null) {
			csrBytes = streamContents;
		}

		PKCS10CertificationRequest csr = new PKCS10CertificationRequest(csrBytes);

		return csr;
	}
	
	public void loadCertificatesToKeyStore(String keyStoreLocation, String keyStorePassword, String certificatePath, String alias) {
		KeyStore keyStore = loadKeyStore(keyStoreLocation, keyStorePassword.toCharArray());
		X509Certificate[] certificates = loadCertificates(certificatePath);
		X509Certificate cert = certificates[0];
		try {
			keyStore.setCertificateEntry(alias, cert);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
	}

	// Wczytuje certyfikaty z pliku o podanej nazwie
	public X509Certificate[] loadCertificates(String filePath) {
		try {
			InputStream inputStream = new FileInputStream(filePath);
			byte[] certificatesAsArrayOfBytes = null;

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				byte[] buffer = new byte[2048];
				int read = 0;

				while ((read = inputStream.read(buffer)) != -1) {
					baos.write(buffer, 0, read);
				}
				certificatesAsArrayOfBytes = baos.toByteArray();
			} finally {
				baos.close();
				inputStream.close();
			}

			inputStream = new ByteArrayInputStream(certificatesAsArrayOfBytes);

			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509", "BC");

			Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(inputStream);

			List<X509Certificate> loadedCertificates = new ArrayList<X509Certificate>();

			for (Iterator<? extends Certificate> iterator = certificates.iterator(); iterator.hasNext();) {
				X509Certificate loadedCertificate = (X509Certificate) iterator.next();

				if (loadedCertificate != null) {
					loadedCertificates.add(loadedCertificate);
				}
			}
			inputStream.close();

			return loadedCertificates.toArray(new X509Certificate[loadedCertificates.size()]);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Wczytuje z pliku o podanej �cie�ce bajty
	public static byte[] readBytesFromFile(String filePath) throws IOException {
		InputStream is = new FileInputStream(filePath);
		ByteArrayOutputStream baos = null;

		try {
			baos = new ByteArrayOutputStream();

			byte[] buffer = new byte[2048];
			int read = 0;

			while ((read = is.read(buffer)) != -1) {
				baos.write(buffer, 0, read);
			}

			return baos.toByteArray();
		} finally {
			baos.close();
			is.close();
		}
	}

	// Podpisuje certificate sign request
	public void signCsr(String alias, String keyPassword, String keyStorePath, String kayStorePassword, String csrFilePath, String replyFilePath) {
		try {
			KeyStore keyStore = getKeyStore(keyStorePath, kayStorePassword);

			PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, keyPassword.toCharArray());
			Certificate[] certs = keyStore.getCertificateChain(alias);

			PKCS10CertificationRequest pkcs10Csr = loadCsrFromFile(csrFilePath);

			X509Certificate[] signingChain = orderX509CertChain(convertCertificatesToX509(certs));
			X509Certificate signingCert = signingChain[0];

			X500Name subject = pkcs10Csr.getSubject();
			X500Name issuer = X500Name.getInstance(BCStyle.INSTANCE, signingCert.getSubjectX500Principal().getEncoded());
			long oneYearMillis = 365 * 24 * 60 * 60 * 1000;
			PublicKey publicKey = new JcaPKCS10CertificationRequest(pkcs10Csr).getPublicKey();

			X509Certificate caReplyCert = generateX509Version3(subject, issuer, oneYearMillis, publicKey, privateKey);

			X509Certificate[] caReplyChain = new X509Certificate[signingChain.length + 1];

			caReplyChain[0] = caReplyCert;

			for (int i = 0; i < signingChain.length; i++) {
				caReplyChain[i + 1] = signingChain[i];
			}

			byte[] caCertEncoded = getCertsEncodedPkcs7(caReplyChain);

			OutputStream fos = new FileOutputStream(replyFilePath);
			fos.write(caCertEncoded);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Zamienia certyfikat na x509
	public static X509Certificate convertCertificateToX509(Certificate certIn) {
		CertificateFactory cf;
		try {
			cf = CertificateFactory.getInstance("X.509", "BC");
			ByteArrayInputStream bais = new ByteArrayInputStream(certIn.getEncoded());
			return (X509Certificate) cf.generateCertificate(bais);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// Zamienia tablic� certyfikat�w na x509
	public static X509Certificate[] convertCertificatesToX509(Certificate[] certsIn) {
		if (certsIn == null) {
			return new X509Certificate[0];
		}

		X509Certificate[] result = new X509Certificate[certsIn.length];
		for (int i = 0; i < certsIn.length; i++) {
			result[i] = convertCertificateToX509(certsIn[i]);
		}

		return result;
	}

	// Uszeregowuje certyfikaty w odpowiedniej kolejno�ci
	public X509Certificate[] orderX509CertChain(X509Certificate certs[]) {
		if (certs == null) {
			return new X509Certificate[0];
		}

		if (certs.length <= 1) {
			return certs;
		}

		// Lista mozliwych �cie�ek certyfikat�w
		ArrayList<ArrayList<X509Certificate>> paths = new ArrayList<ArrayList<X509Certificate>>();

		// dla ka�dej sciezki
		for (int i = 0; i < certs.length; i++) {
			ArrayList<X509Certificate> path = new ArrayList<X509Certificate>();
			X509Certificate issuerCert = certs[i];
			path.add(issuerCert);

			X509Certificate newIssuer = null;

			while ((newIssuer = findIssuedCert(issuerCert, certs)) != null) {
				issuerCert = newIssuer;
				path.add(0, newIssuer);
			}

			paths.add(path);
		}

		ArrayList<X509Certificate> longestPath = paths.get(0);
		for (int i = 1; i < paths.size(); i++) {
			ArrayList<X509Certificate> path = paths.get(i);
			if (path.size() > longestPath.size()) {
				longestPath = path;
			}
		}

		return longestPath.toArray(new X509Certificate[longestPath.size()]);
	}

	// Wyszukuje certyfikat wydawcy
	private X509Certificate findIssuedCert(X509Certificate issuerCert, X509Certificate[] certs) {
		for (int i = 0; i < certs.length; i++) {
			X509Certificate cert = certs[i];

			if ((issuerCert.getSubjectX500Principal().equals(cert.getSubjectX500Principal()))
					&& (issuerCert.getIssuerX500Principal().equals(cert.getIssuerX500Principal()))) {
				continue;
			}

			if (issuerCert.getSubjectX500Principal().equals(cert.getIssuerX500Principal())) {
				return cert;
			}
		}

		return null;
	}

	// Zwraca certyfikaty jako tablic� bajt�w do zapisu
	public byte[] getCertsEncodedPkcs7(X509Certificate[] certs) {
		try {
			ArrayList<Certificate> encodedCerts = new ArrayList<Certificate>();

			for (int i = 0; i < certs.length; i++) {
				encodedCerts.add(certs[i]);
			}

			CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");

			CertPath cp = cf.generateCertPath(encodedCerts);

			return cp.getEncoded("PkiPath");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void signFile(String keyStoreLocation, String keyStorePassword) {
		try {
			KeyStore keyStore = loadKeyStore(keyStoreLocation, keyStorePassword.toCharArray());
			Signature dsa = Signature.getInstance("SHA1withDSA", "SUN");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
