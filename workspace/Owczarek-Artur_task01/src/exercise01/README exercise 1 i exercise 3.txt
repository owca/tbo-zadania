UWAGA: W plikach polityk bezpiecze�stwa nale�y zmieni� �cie�ki dost�pu, aby przyk�ady dzia�a�y.

1.1
Pr�ba uruchomienia appletu niekoniecznie musi si� zako�czy� niepowodzeniem. Je�li uruchomimy go z poziomu Eclipse, to zostanie automatycznie wygenerowany plik java.policy.application (je�li ju� taki nie istnieje), kt�ry domy�lnie pozwala na wszystko. Je�li jednak uruchomimy ten aplet z poziomu przegl�darki, to domy�lny poziom restrykcji uniemo�liwi dost�p do pliku i wi�kszo�ci w�a�ciwo�ci (�eby program zadzia�a�, potrzebne s� prawa do wszystkich w�a�ciwo�ci; nie wiem czy tak jest wsz�dzie, ale m�j plik z polityk� bezpiecze�stwa umo�liwia apletom na dost�p do niekt�rych w�a�ciwo�ci).

1.2
Do uruchomienia przyk�adu trzeba poda� plik z polityk� bezpiecze�stwa (poprzedzony dodatkowym znakiem '=', aby zast�pi� domy�lny) i wymusi� wykorzystanie security managera. W przeciwnym wypadku przyk�ad nie zadzia�a.
-Djava.security.policy==src/exercise02/java.policy -Djava.security.manager
Nie da si� napisa� polityki bezpiecze�stwa zabraniaj�cej dost�pu. Mo�na napisa� tylko tak�, kt�ra daje prawa dost�pu.

1.3
Zadanie 1.3 znajduje si� w pakiecie exercise01. Uwa�am, �e nie ma sensu tworzy� dwa razy praktycznie identycznego kodu.
Plik polityki znajduje si� w scr/java.policy.applet. Zostanie on zaczytany przez Eclipse automatycznie.