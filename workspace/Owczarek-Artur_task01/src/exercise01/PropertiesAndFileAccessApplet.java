package exercise01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JApplet;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PropertiesAndFileAccessApplet extends JApplet {
	private JTextArea textContainer;

	@Override
	public void init() {
		createComponents();
		deployComponents();
	}

	@Override
	public void start() {
		try {
			listProperties();
		} catch (SecurityException e) {
			textContainer.append("Brak uprawnień do odczytu/zapisu właściwości systemowych!\n");
			textContainer.append(e.getMessage());
			textContainer.append("\n");
		}
		try {
			listFile("C:\\Windows\\system32\\drivers\\etc\\hosts");
		} catch (SecurityException e) {
			textContainer.append("Brak uprawnień do odczytu pliku C:\\Windows\\system32\\drivers\\etc\\hosts\n");
			textContainer.append(e.getMessage());
		}
	}

	private void createComponents() {
		textContainer = new JTextArea();
	}

	private void deployComponents() {
		JScrollPane scrollPane = new JScrollPane(textContainer);
		add(scrollPane);
	}

	private void listProperties() {
		Properties properties = System.getProperties();

		for (Map.Entry<Object, Object> property : properties.entrySet()) {
			textContainer.append(property.toString());
			textContainer.append("\n");
		}
	}

	private void listFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner skaner = new Scanner(file);
			while (skaner.hasNextLine()) {
				String line = skaner.nextLine();
				textContainer.append(line);
				textContainer.append("\n");
			}
			skaner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
