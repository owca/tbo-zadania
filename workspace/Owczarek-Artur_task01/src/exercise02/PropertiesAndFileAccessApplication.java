package exercise02;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilePermission;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PropertiesAndFileAccessApplication extends JFrame {
	private JTextArea textContainer;

	public static void main(String[] args) {
		FilePermission p = new FilePermission("C:\\Windows\\system32\\drivers\\etc\\hosts", "read");
		System.getSecurityManager().checkPermission(p);
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new PropertiesAndFileAccessApplication();
			}
		});
	}

	public PropertiesAndFileAccessApplication() {
		createComponents();
		deployComponents();
		start();
	}

	private void start() {
		listProperties();
		listFile("C:\\Windows\\system32\\drivers\\etc\\hosts");
	}

	private void createComponents() {
		textContainer = new JTextArea();
	}

	private void deployComponents() {
		JScrollPane scrollPane = new JScrollPane(textContainer);
		add(scrollPane);
		setSize(640, 480);
		setTitle("fff");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void listProperties() {
		Properties properties = System.getProperties();

		for (Map.Entry<Object, Object> property : properties.entrySet()) {
			textContainer.append(property.toString());
			textContainer.append("\n");
		}
	}

	private void listFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner skaner = new Scanner(file);
			while (skaner.hasNextLine()) {
				String line = skaner.nextLine();
				textContainer.append(line);
				textContainer.append("\n");
			}
			skaner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
