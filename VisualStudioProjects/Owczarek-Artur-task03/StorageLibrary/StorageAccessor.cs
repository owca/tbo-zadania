﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Forms;

namespace StorageLibrary
{
    public class StorageAccessor
    {
        public void CreateDirectoryAndSaveFile(string directoryName, string fileName, string fileContent)
        {
            IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForAssembly();
            store.CreateDirectory(directoryName);
            String filePath = directoryName + "/" + fileName;
            IsolatedStorageFileStream fileStream = store.OpenFile(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite);
            StreamWriter writer = new StreamWriter(fileStream);
            writer.Write(fileContent);
            writer.Close();
            store.Close();
        }

        public string ReadFile(string path)
        {
            IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForAssembly();
            IsolatedStorageFileStream fileStream = store.OpenFile(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            StreamReader reader = new StreamReader(fileStream);
            string text = reader.ReadToEnd();
            store.Close();
            return text;
        }

        public long AvailableFreeSpace
        {
            get
            {
                IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForAssembly();
                return store.AvailableFreeSpace;
            }
            set
            {
            }
        }

        public TreeNode CreateDirectoryNode(string directoryPath, IsolatedStorageFile storage)
        {
            bool closingStorage = false;
            if (storage == null)
            {
                closingStorage = true;
                storage = IsolatedStorageFile.GetUserStoreForApplication();
            }

            TreeNode directoryNode = null;
            string[] subdirectories = null;
            string[] files = null;
            if (directoryPath != null)
            {
                directoryNode = new TreeNode(directoryPath);
                subdirectories = storage.GetDirectoryNames(directoryPath);
                files = storage.GetFileNames(directoryPath);
            }
            else
            {
                directoryNode = new TreeNode("/");
                subdirectories = storage.GetDirectoryNames();
                files = storage.GetFileNames();
            }

            foreach (string directory in subdirectories)
            {
                directoryNode.Nodes.Add(CreateDirectoryNode(directoryPath + "/" + directory, storage));
            }

            foreach (string file in files)
            {
                directoryNode.Nodes.Add(new TreeNode(directoryPath + "/" + file));
            }

            if (closingStorage)
            {
                storage.Close();
            }
            return directoryNode;
        }
    }
}
