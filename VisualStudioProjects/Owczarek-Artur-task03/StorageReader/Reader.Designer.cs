﻿namespace StorageEditor
{
    partial class Reader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.quota = new System.Windows.Forms.TextBox();
            this.quotaLabel = new System.Windows.Forms.Label();
            this.directory = new System.Windows.Forms.TextBox();
            this.file = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.read = new System.Windows.Forms.Button();
            this.fileContent = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // quota
            // 
            this.quota.Location = new System.Drawing.Point(54, 12);
            this.quota.Name = "quota";
            this.quota.Size = new System.Drawing.Size(408, 20);
            this.quota.TabIndex = 1;
            // 
            // quotaLabel
            // 
            this.quotaLabel.AutoSize = true;
            this.quotaLabel.Location = new System.Drawing.Point(12, 15);
            this.quotaLabel.Name = "quotaLabel";
            this.quotaLabel.Size = new System.Drawing.Size(43, 13);
            this.quotaLabel.TabIndex = 2;
            this.quotaLabel.Text = "Miejsce";
            this.quotaLabel.Click += new System.EventHandler(this.quotaLabel_Click);
            // 
            // directory
            // 
            this.directory.Location = new System.Drawing.Point(54, 48);
            this.directory.Name = "directory";
            this.directory.Size = new System.Drawing.Size(408, 20);
            this.directory.TabIndex = 3;
            // 
            // file
            // 
            this.file.Location = new System.Drawing.Point(54, 74);
            this.file.Name = "file";
            this.file.Size = new System.Drawing.Size(408, 20);
            this.file.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Plik";
            // 
            // read
            // 
            this.read.Location = new System.Drawing.Point(183, 395);
            this.read.Name = "read";
            this.read.Size = new System.Drawing.Size(75, 23);
            this.read.TabIndex = 7;
            this.read.Text = "Wczytaj";
            this.read.UseVisualStyleBackColor = true;
            this.read.Click += new System.EventHandler(this.read_Click);
            // 
            // fileContent
            // 
            this.fileContent.Location = new System.Drawing.Point(12, 100);
            this.fileContent.Multiline = true;
            this.fileContent.Name = "fileContent";
            this.fileContent.Size = new System.Drawing.Size(443, 272);
            this.fileContent.TabIndex = 8;
            // 
            // Reader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 430);
            this.Controls.Add(this.fileContent);
            this.Controls.Add(this.read);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.file);
            this.Controls.Add(this.directory);
            this.Controls.Add(this.quotaLabel);
            this.Controls.Add(this.quota);
            this.Name = "Reader";
            this.Text = "Reader";
            this.Load += new System.EventHandler(this.Reader_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox quota;
        private System.Windows.Forms.Label quotaLabel;
        private System.Windows.Forms.TextBox directory;
        private System.Windows.Forms.TextBox file;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button read;
        private System.Windows.Forms.TextBox fileContent;
    }
}