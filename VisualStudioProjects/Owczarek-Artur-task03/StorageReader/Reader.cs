﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using StorageLibrary;

namespace StorageEditor
{
    public partial class Reader : Form
    {
        private StorageAccessor storage = new StorageAccessor();

        public Reader()
        {
            InitializeComponent();
        }

        

        private void Reader_Load(object sender, EventArgs e)
        {
            quota.Text = storage.AvailableFreeSpace.ToString();

            //TreeNode root = storage.CreateDirectoryNode(null, null);
            //storageTree.Nodes.Add(root);
        }

        private void read_Click(object sender, EventArgs e)
        {
            string directoryPath = directory.Text;
            string filePath = file.Text;
            String content = storage.ReadFile(directoryPath + "\\" + filePath);
            fileContent.Text = content;
        }

        private void quotaLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
