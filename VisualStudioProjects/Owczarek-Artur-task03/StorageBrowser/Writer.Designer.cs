﻿namespace StorageBrowser
{
    partial class Writer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.directoryField = new System.Windows.Forms.TextBox();
            this.fileLabel = new System.Windows.Forms.Label();
            this.fileField = new System.Windows.Forms.TextBox();
            this.content = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folder";
            // 
            // directoryField
            // 
            this.directoryField.Location = new System.Drawing.Point(58, 10);
            this.directoryField.Name = "directoryField";
            this.directoryField.Size = new System.Drawing.Size(214, 20);
            this.directoryField.TabIndex = 1;
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point(13, 42);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(24, 13);
            this.fileLabel.TabIndex = 2;
            this.fileLabel.Text = "Plik";
            // 
            // fileField
            // 
            this.fileField.Location = new System.Drawing.Point(58, 39);
            this.fileField.Name = "fileField";
            this.fileField.Size = new System.Drawing.Size(214, 20);
            this.fileField.TabIndex = 3;
            // 
            // content
            // 
            this.content.Location = new System.Drawing.Point(16, 65);
            this.content.Multiline = true;
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(256, 160);
            this.content.TabIndex = 4;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(110, 231);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 5;
            this.save.Text = "Zapisz";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // Writer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.save);
            this.Controls.Add(this.content);
            this.Controls.Add(this.fileField);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.directoryField);
            this.Controls.Add(this.label1);
            this.Name = "Writer";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox directoryField;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.TextBox fileField;
        private System.Windows.Forms.TextBox content;
        private System.Windows.Forms.Button save;
    }
}

