﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StorageLibrary;

namespace StorageBrowser
{
    public partial class Writer : Form
    {
        private StorageAccessor store = new StorageAccessor();

        public Writer()
        {
            InitializeComponent();
        }

        private void save_Click(object sender, EventArgs e)
        {
            String directoryName = directoryField.Text;
            String fileName = fileField.Text;
            String fileContent = content.Text;
            store.CreateDirectoryAndSaveFile(directoryName, fileName, fileContent);
            TreeNode tree = store.CreateDirectoryNode(null, null);
        }
    }
}
