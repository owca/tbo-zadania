﻿namespace Owczarek_Artur_task06
{
    partial class CryptographyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.hashInput = new System.Windows.Forms.TextBox();
            this.calculateMd5 = new System.Windows.Forms.Button();
            this.calculateSha1 = new System.Windows.Forms.Button();
            this.hashOutput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cnTextBox = new System.Windows.Forms.TextBox();
            this.generateKeyAndCertificate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.importCertificate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Podpisz = new System.Windows.Forms.Button();
            this.signCertificateSignature = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.generateAES = new System.Windows.Forms.Button();
            this.generatedAES = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.signXml = new System.Windows.Forms.Button();
            this.verifyXml = new System.Windows.Forms.Button();
            this.xmlKey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Funkcje skrótu";
            // 
            // hashInput
            // 
            this.hashInput.Location = new System.Drawing.Point(16, 29);
            this.hashInput.Name = "hashInput";
            this.hashInput.Size = new System.Drawing.Size(262, 20);
            this.hashInput.TabIndex = 1;
            // 
            // calculateMd5
            // 
            this.calculateMd5.Location = new System.Drawing.Point(16, 55);
            this.calculateMd5.Name = "calculateMd5";
            this.calculateMd5.Size = new System.Drawing.Size(51, 23);
            this.calculateMd5.TabIndex = 2;
            this.calculateMd5.Text = "MD5";
            this.calculateMd5.UseVisualStyleBackColor = true;
            this.calculateMd5.Click += new System.EventHandler(this.calculateMd5_Click);
            // 
            // calculateSha1
            // 
            this.calculateSha1.Location = new System.Drawing.Point(73, 54);
            this.calculateSha1.Name = "calculateSha1";
            this.calculateSha1.Size = new System.Drawing.Size(51, 23);
            this.calculateSha1.TabIndex = 3;
            this.calculateSha1.Text = "SHA1";
            this.calculateSha1.UseVisualStyleBackColor = true;
            this.calculateSha1.Click += new System.EventHandler(this.calculateSha1_Click);
            // 
            // hashOutput
            // 
            this.hashOutput.Location = new System.Drawing.Point(16, 83);
            this.hashOutput.Name = "hashOutput";
            this.hashOutput.Size = new System.Drawing.Size(262, 20);
            this.hashOutput.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "CN";
            // 
            // cnTextBox
            // 
            this.cnTextBox.Location = new System.Drawing.Point(44, 138);
            this.cnTextBox.Name = "cnTextBox";
            this.cnTextBox.Size = new System.Drawing.Size(234, 20);
            this.cnTextBox.TabIndex = 6;
            // 
            // generateKeyAndCertificate
            // 
            this.generateKeyAndCertificate.Location = new System.Drawing.Point(19, 164);
            this.generateKeyAndCertificate.Name = "generateKeyAndCertificate";
            this.generateKeyAndCertificate.Size = new System.Drawing.Size(166, 23);
            this.generateKeyAndCertificate.TabIndex = 7;
            this.generateKeyAndCertificate.Text = "Generuj klucz i certyfikat";
            this.generateKeyAndCertificate.UseVisualStyleBackColor = true;
            this.generateKeyAndCertificate.Click += new System.EventHandler(this.generateKeyAndCertificate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Generowanie kluczy i certyfikatów";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Import certyfikatu";
            // 
            // importCertificate
            // 
            this.importCertificate.Location = new System.Drawing.Point(146, 207);
            this.importCertificate.Name = "importCertificate";
            this.importCertificate.Size = new System.Drawing.Size(75, 23);
            this.importCertificate.TabIndex = 10;
            this.importCertificate.Text = "Importuj";
            this.importCertificate.UseVisualStyleBackColor = true;
            this.importCertificate.Click += new System.EventHandler(this.importCertificate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 253);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Podpisywanie";
            // 
            // Podpisz
            // 
            this.Podpisz.Location = new System.Drawing.Point(25, 276);
            this.Podpisz.Name = "Podpisz";
            this.Podpisz.Size = new System.Drawing.Size(75, 23);
            this.Podpisz.TabIndex = 14;
            this.Podpisz.Text = "Podpisz";
            this.Podpisz.UseVisualStyleBackColor = true;
            this.Podpisz.Click += new System.EventHandler(this.Podpisz_Click);
            // 
            // signCertificateSignature
            // 
            this.signCertificateSignature.Location = new System.Drawing.Point(25, 305);
            this.signCertificateSignature.Multiline = true;
            this.signCertificateSignature.Name = "signCertificateSignature";
            this.signCertificateSignature.Size = new System.Drawing.Size(253, 20);
            this.signCertificateSignature.TabIndex = 15;
            this.signCertificateSignature.TextChanged += new System.EventHandler(this.signCertificateSignature_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Klucz symetryczny";
            // 
            // generateAES
            // 
            this.generateAES.Location = new System.Drawing.Point(28, 363);
            this.generateAES.Name = "generateAES";
            this.generateAES.Size = new System.Drawing.Size(90, 23);
            this.generateAES.TabIndex = 17;
            this.generateAES.Text = "Generuj AES";
            this.generateAES.UseVisualStyleBackColor = true;
            this.generateAES.Click += new System.EventHandler(this.generateAES_Click);
            // 
            // generatedAES
            // 
            this.generatedAES.Location = new System.Drawing.Point(134, 365);
            this.generatedAES.Name = "generatedAES";
            this.generatedAES.Size = new System.Drawing.Size(144, 20);
            this.generatedAES.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(349, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Sygnatury xml";
            // 
            // signXml
            // 
            this.signXml.Location = new System.Drawing.Point(352, 80);
            this.signXml.Name = "signXml";
            this.signXml.Size = new System.Drawing.Size(75, 23);
            this.signXml.TabIndex = 20;
            this.signXml.Text = "Podpisz";
            this.signXml.UseVisualStyleBackColor = true;
            this.signXml.Click += new System.EventHandler(this.signXml_Click);
            // 
            // verifyXml
            // 
            this.verifyXml.Location = new System.Drawing.Point(467, 80);
            this.verifyXml.Name = "verifyXml";
            this.verifyXml.Size = new System.Drawing.Size(75, 23);
            this.verifyXml.TabIndex = 21;
            this.verifyXml.Text = "Weryfikuj";
            this.verifyXml.UseVisualStyleBackColor = true;
            this.verifyXml.Click += new System.EventHandler(this.verifyXml_Click);
            // 
            // xmlKey
            // 
            this.xmlKey.Location = new System.Drawing.Point(458, 40);
            this.xmlKey.Name = "xmlKey";
            this.xmlKey.Size = new System.Drawing.Size(262, 20);
            this.xmlKey.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(352, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Klucz";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(352, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "CMS";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(355, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Zrób robotę";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CryptographyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 409);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.xmlKey);
            this.Controls.Add(this.verifyXml);
            this.Controls.Add(this.signXml);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.generatedAES);
            this.Controls.Add(this.generateAES);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.signCertificateSignature);
            this.Controls.Add(this.Podpisz);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.importCertificate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.generateKeyAndCertificate);
            this.Controls.Add(this.cnTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hashOutput);
            this.Controls.Add(this.calculateSha1);
            this.Controls.Add(this.calculateMd5);
            this.Controls.Add(this.hashInput);
            this.Controls.Add(this.label1);
            this.Name = "CryptographyForm";
            this.Text = "Kryptografia w C#";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox hashInput;
        private System.Windows.Forms.Button calculateMd5;
        private System.Windows.Forms.Button calculateSha1;
        private System.Windows.Forms.TextBox hashOutput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cnTextBox;
        private System.Windows.Forms.Button generateKeyAndCertificate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button importCertificate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Podpisz;
        public System.Windows.Forms.TextBox signCertificateSignature;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button generateAES;
        private System.Windows.Forms.TextBox generatedAES;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button signXml;
        private System.Windows.Forms.Button verifyXml;
        private System.Windows.Forms.TextBox xmlKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
    }
}

