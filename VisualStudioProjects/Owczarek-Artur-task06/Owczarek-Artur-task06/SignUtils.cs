﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Owczarek_Artur_task06
{
    class SignUtils
    {
        const string OID_SHA1 = "1.3.14.3.2.26";
        const string OID_MD5 = "1.2.840.113549.2.5";
        const string SIMPLE_NAME_SHA1 = "SHA1";
        const string SIMPLE_NAME_MD5 = "MD5";

        public static string Sign(string filePath)
        {
            byte[] data = File.ReadAllBytes(filePath);
           
            byte[] signature = GenerateRSASignature(data);
            return Utils.ConvertBytesArrayToPrettyString(signature);
        }

        private static byte[] GenerateRSASignature(byte[] data)
        {
            HashAlgorithm hash = SHA1.Create();
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters rsaParameters = RSA.ExportParameters(false);
            byte[] hashValue = hash.ComputeHash(data);
            byte[] signature = RSA.SignHash(hashValue, OID_SHA1);
            return signature;
        }

        private bool verifyRSASignature(byte[] data, byte[] signature, RSAParameters publicKey)
        {
            HashAlgorithm hash = SHA1.Create();
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(publicKey);
            byte[] hashValue = hash.ComputeHash(data);
            return rsa.VerifyHash(hashValue, OID_SHA1, signature);
        }

        public static string GenMacMd5(string message)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters rsaParameters = RSA.ExportParameters(false);
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(rsaParameters.ToString());

            HMACMD5 hmacmd5 = new HMACMD5(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);
            byte[] hashmessage = hmacmd5.ComputeHash(messageBytes);
            return Utils.ConvertBytesArrayToPrettyString(hashmessage);
        }

        public static string GenMacSha1(string message)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters rsaParameters = RSA.ExportParameters(false);
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(rsaParameters.ToString());

            HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);
            byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
            return Utils.ConvertBytesArrayToPrettyString(hashmessage);
        }
    }
}
