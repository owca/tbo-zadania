﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace Owczarek_Artur_task06
{
    public partial class CryptographyForm : Form
    {
        public CryptographyForm()
        {
            InitializeComponent();
        }

        private void calculateMd5_Click(object sender, EventArgs e)
        {
            hashOutput.Text = HashUtils.GetMd5Hash(hashInput.Text);
        }

        private void calculateSha1_Click(object sender, EventArgs e)
        {
            hashOutput.Text = HashUtils.GetSha1Hash(hashInput.Text);
        }

        private void generateKeyAndCertificate_Click(object sender, EventArgs e)
        {
            KeyUtils.GenerateKeysAndSaveToStore(cnTextBox.Text);
        }

        private void importCertificate_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                KeyUtils.LoadAndAddCertificate(openFileDialog.FileName);
            }
        }

        private void Podpisz_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String signature = SignUtils.Sign(openFileDialog.FileName);
                signCertificateSignature.Text = signature;
            }
        }

        private void signCertificateSignature_TextChanged(object sender, EventArgs e)
        {

        }

        private void generateAES_Click(object sender, EventArgs e)
        {
            generatedAES.Text = KeyUtils.GenerateAes();
        }

        private void signXml_Click(object sender, EventArgs e)
        {
            string fileToSign = null;
            string signedFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileToSign = openFileDialog.FileName;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = fileToSign;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                signedFile = saveFileDialog.FileName;
            }

            XmlUtils.signXmlDocument(fileToSign, signedFile, xmlKey.Text);
        }

        private void verifyXml_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String fileToVerify = openFileDialog.FileName;
                bool result = XmlUtils.verifySignature(fileToVerify, xmlKey.Text);

                if (result)
                {
                    MessageBox.Show("Weryfikacja się powiodła");
                }
                else
                {
                    MessageBox.Show("Weryfikacja się nie powiodła");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fileToSign = null;
            string envelopeFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileToSign = openFileDialog.FileName;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                envelopeFile = saveFileDialog.FileName;
            }

            byte[] inputBytes = File.ReadAllBytes(fileToSign);
            X509Certificate2 cert = KeyUtils.GenerateCertificate("myCert");
            byte[] envelope = CmsUtils.EncryptMsg(inputBytes, cert);

            File.WriteAllBytes(envelopeFile, envelope);
        }
    }
}
