﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Owczarek_Artur_task06
{
    class HashUtils
    {
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Utils.ConvertBytesArrayToPrettyString(data);
        }

        public static string GetSha1Hash(string input)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();

            byte[] data = sha.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Utils.ConvertBytesArrayToPrettyString(data);
        }
    }
}
