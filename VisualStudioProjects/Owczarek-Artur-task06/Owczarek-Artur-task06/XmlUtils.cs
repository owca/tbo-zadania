﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;

namespace Owczarek_Artur_task06
{
    class XmlUtils
    {
        public static void signXmlDocument(String documentPath, String signedDocumentPath, String KeyContainerName)
        {
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = KeyContainerName;
            //cspParams.KeyContainerName = "XML_DSIG_RSA_KEY";
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(documentPath);

            SignedXml signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = rsaKey;

            Reference reference = new Reference();
            reference.Uri = "";

            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);

            signedXml.AddReference(reference);
            signedXml.ComputeSignature();
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            xmlDoc.Save(signedDocumentPath);
        }

        public static bool verifySignature(String signedDocumentPath, String KeyContainerName)
        {
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = KeyContainerName;
            //cspParams.KeyContainerName = "XML_DSIG_RSA_KEY";
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(signedDocumentPath);

            bool result = VerifyXml(xmlDoc, rsaKey);

            return result;
        }

        public static Boolean VerifyXml(XmlDocument Doc, RSA Key)
        {
            SignedXml signedXml = new SignedXml(Doc);

            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

            if (nodeList.Count <= 0)
            {
                throw new CryptographicException("W dokumencie brak jest węzła z sygnaturą.");
            }

            if (nodeList.Count >= 2)
            {
                throw new CryptographicException("W dokumencie znaleziono za dużo sygnatur");
            }

            signedXml.LoadXml((XmlElement)nodeList[0]);

            return signedXml.CheckSignature(Key);
        }

        byte[] CreateCMSEnvelopedData(byte[] input, X509Certificate2 recipientEntry)
        {
            ContentInfo content = new ContentInfo(input);
            EnvelopedCms envelopedData = new EnvelopedCms(content);
            CmsRecipient recipient = new CmsRecipient(
            SubjectIdentifierType.IssuerAndSerialNumber, recipientEntry);
            envelopedData.Encrypt(recipient);
            return envelopedData.Encode();
        }


    }
}
