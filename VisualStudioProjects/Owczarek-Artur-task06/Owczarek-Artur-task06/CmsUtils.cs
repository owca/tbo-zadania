﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;

namespace Owczarek_Artur_task06
{
    class CmsUtils
    {
        public static byte[] EncryptMsg(Byte[] msg, X509Certificate2 recipientCert)
        {
            ContentInfo contentInfo = new ContentInfo(msg);
            EnvelopedCms envelopedCms = new EnvelopedCms(contentInfo);

            CmsRecipient recip1 = new CmsRecipient(SubjectIdentifierType.IssuerAndSerialNumber, recipientCert);

            Console.Write(
                "Encrypting data for a single recipient of " +
                "subject name {0} ... ",
                recip1.Certificate.SubjectName.Name);
            envelopedCms.Encrypt(recip1);
            Console.WriteLine("Done.");

            return envelopedCms.Encode();
        }
    }
}
