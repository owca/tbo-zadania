﻿namespace View
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.content = new System.Windows.Forms.TextBox();
            this.writeToFile = new System.Windows.Forms.Button();
            this.readFromFile = new System.Windows.Forms.Button();
            this.readFromIS = new System.Windows.Forms.Button();
            this.writeToIS = new System.Windows.Forms.Button();
            this.readFromRegistry = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Location = new System.Drawing.Point(12, 12);
            this.content.Multiline = true;
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(401, 96);
            this.content.TabIndex = 0;
            // 
            // writeToFile
            // 
            this.writeToFile.Location = new System.Drawing.Point(12, 114);
            this.writeToFile.Name = "writeToFile";
            this.writeToFile.Size = new System.Drawing.Size(197, 23);
            this.writeToFile.TabIndex = 1;
            this.writeToFile.Text = "Zapisz do pliku";
            this.writeToFile.UseVisualStyleBackColor = true;
            this.writeToFile.Click += new System.EventHandler(this.writeToFile_Click);
            // 
            // readFromFile
            // 
            this.readFromFile.Location = new System.Drawing.Point(216, 114);
            this.readFromFile.Name = "readFromFile";
            this.readFromFile.Size = new System.Drawing.Size(197, 23);
            this.readFromFile.TabIndex = 2;
            this.readFromFile.Text = "Wczytaj z pliku";
            this.readFromFile.UseVisualStyleBackColor = true;
            this.readFromFile.Click += new System.EventHandler(this.readFromFile_Click);
            // 
            // readFromIS
            // 
            this.readFromIS.Location = new System.Drawing.Point(216, 143);
            this.readFromIS.Name = "readFromIS";
            this.readFromIS.Size = new System.Drawing.Size(197, 23);
            this.readFromIS.TabIndex = 4;
            this.readFromIS.Text = "Wczytaj z IS";
            this.readFromIS.UseVisualStyleBackColor = true;
            this.readFromIS.Click += new System.EventHandler(this.readFromIS_Click);
            // 
            // writeToIS
            // 
            this.writeToIS.Location = new System.Drawing.Point(12, 143);
            this.writeToIS.Name = "writeToIS";
            this.writeToIS.Size = new System.Drawing.Size(197, 23);
            this.writeToIS.TabIndex = 3;
            this.writeToIS.Text = "Zapisz do IS";
            this.writeToIS.UseVisualStyleBackColor = true;
            this.writeToIS.Click += new System.EventHandler(this.writeToIS_Click);
            // 
            // readFromRegistry
            // 
            this.readFromRegistry.Location = new System.Drawing.Point(12, 172);
            this.readFromRegistry.Name = "readFromRegistry";
            this.readFromRegistry.Size = new System.Drawing.Size(197, 23);
            this.readFromRegistry.TabIndex = 5;
            this.readFromRegistry.Text = "Wczytaj klucz z rejestru";
            this.readFromRegistry.UseVisualStyleBackColor = true;
            this.readFromRegistry.Click += new System.EventHandler(this.readFromRegistry_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 202);
            this.Controls.Add(this.readFromRegistry);
            this.Controls.Add(this.readFromIS);
            this.Controls.Add(this.writeToIS);
            this.Controls.Add(this.readFromFile);
            this.Controls.Add(this.writeToFile);
            this.Controls.Add(this.content);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox content;
        private System.Windows.Forms.Button writeToFile;
        private System.Windows.Forms.Button readFromFile;
        private System.Windows.Forms.Button readFromIS;
        private System.Windows.Forms.Button writeToIS;
        private System.Windows.Forms.Button readFromRegistry;
    }
}

