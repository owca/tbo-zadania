﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Library;
using LogInModule;

namespace View
{
    public partial class Form1 : Form
    {
        //LibraryMethods library = new LibraryMethods();
        LibraryAttributes library = new LibraryAttributes();

        public Form1()
        {
            Auth authenticationForm = new Auth();
            Thread.CurrentPrincipal = authenticationForm.GetPrincipal();
            InitializeComponent();
        }

        private void writeToFile_Click(object sender, EventArgs e)
        {
            library.WriteToFileOnDisc(content.Text);
        }

        private void readFromFile_Click(object sender, EventArgs e)
        {
            content.Text = library.ReadFileFromDisc();
        }

        private void writeToIS_Click(object sender, EventArgs e)
        {
            library.WriteToIsolatedStorageFile(content.Text);
        }

        private void readFromIS_Click(object sender, EventArgs e)
        {
            content.Text = library.ReadFromIsolatedStorageFile();
        }

        private void readFromRegistry_Click(object sender, EventArgs e)
        {
            content.Text = library.ReadRegistryKey();
        }
    }
}
