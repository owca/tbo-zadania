﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogInModule.Domain;
using NHibernate.Cfg;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Security.Principal;

namespace LogInModule
{
    class Authentication
    {
        private Configuration config;
        private ISessionFactory factory;

        public Authentication()
        {
            config = new Configuration();
            config.Configure();
            config.AddAssembly(typeof(User).Assembly);
            //config.AddAssembly(typeof(Role).Assembly);
            factory = config.BuildSessionFactory();
            createDatabase();
        }

        public GenericPrincipal retrievePrincipal(string login, string password)
        {
            using (var session = factory.OpenSession())
            {
                User user = session.QueryOver<User>().Where(u => u.UserName == login).And(u => u.Password == password).SingleOrDefault();

                if (user != null)
                {
                    List<string> roles = new List<string>();
                    foreach (var role in user.Roles)
                    {
                        roles.Add(role.RoleName);
                    }
                    return new GenericPrincipal(new GenericIdentity(user.UserName), roles.ToArray());
                }
                else
                {
                    return null;
                }
            }
        }

        private void createDatabase()
        {
            new SchemaExport(config).Execute(true, true, false);

            using (var session = factory.OpenSession())
            {
                var adminsRole = new Role();
                var usersRole = new Role();

                adminsRole.RoleName = "admins";
                usersRole.RoleName = "users";

                session.Save(adminsRole);
                session.Save(usersRole);

                var user1 = new User();
                var admin1 = new User();
                var superadmin = new User();
                var nogrup = new User();

                user1.UserName = "user1";
                admin1.UserName = "admin1";
                superadmin.UserName = "superadmin";
                nogrup.UserName = "nogroup";

                user1.Password = "password";
                admin1.Password = "password";
                superadmin.Password = "password";
                nogrup.Password = "password";

                user1.Roles = new HashSet<Role>() { usersRole };
                admin1.Roles = new HashSet<Role>() { adminsRole };
                superadmin.Roles = new HashSet<Role>() { adminsRole, usersRole };

                session.Save(user1);
                session.Save(admin1);
                session.Save(superadmin);
                session.Save(nogrup);

                session.Flush();
            }
        }
    }
}
