﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

namespace LogInModule
{
    public partial class Auth : Form
    {
        Authentication authentication = new Authentication();

        public Auth()
        {
            InitializeComponent();
        }

        public GenericPrincipal GetPrincipal()
        {
            if (this.ShowDialog() == DialogResult.OK)
            {
                GenericPrincipal principal = authentication.retrievePrincipal(user.Text, password.Text);
                if (principal == null)
                {
                    principal = GetPrincipal();
                }
                return principal;
            }
            else
            {
                return null;
            }
        }
    }
}
