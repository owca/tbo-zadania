﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogInModule.Domain
{
    public class User
    {
        public virtual string UserName
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual ICollection<Role> Roles
        {
            get;
            set;
        }
    }
}
