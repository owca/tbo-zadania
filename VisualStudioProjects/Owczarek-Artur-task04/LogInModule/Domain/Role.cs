﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogInModule.Domain
{
    public class Role
    {
        public virtual string RoleName {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            bool sameName = RoleName == ((Role)obj).RoleName;

            return sameName;
        }

        public override int GetHashCode()
        {
            return RoleName.GetHashCode();
        }
    }
}
