﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Permissions;

namespace Library
{
    public class LibraryMethods
    {
        Library library = new Library();

        public void WriteToFileOnDisc(string text)
        {
            PrincipalPermission permission = new PrincipalPermission(null, "users");
            permission.Demand();
            library.WriteToFileOnDisc(text);
        }

        public string ReadFileFromDisc()
        {
            PrincipalPermission permission = new PrincipalPermission(null, "users");
            permission.Demand();
            return library.ReadFileFromDisc();
        }

        public void WriteToIsolatedStorageFile(string text)
        {
            PrincipalPermission permission = new PrincipalPermission(null, null, true);
            permission.Demand();
            library.WriteToIsolatedStorageFile(text);
        }

        public string ReadFromIsolatedStorageFile()
        {
            PrincipalPermission principalPerm = new PrincipalPermission(null, null, true);
            principalPerm.Demand();
            return library.ReadFromIsolatedStorageFile();
        }

        public string ReadRegistryKey()
        {
            PrincipalPermission permission = new PrincipalPermission(null, "admins");
            permission.Demand();
            return library.ReadRegistryKey();
        }
    }
}
