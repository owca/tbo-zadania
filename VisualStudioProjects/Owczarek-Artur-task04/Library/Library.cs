﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Win32;

namespace Library
{
    public class Library
    {
        private string SOME_FILE = "myfile";

        public string ReadFileFromDisc()
        {
            return File.ReadAllText(SOME_FILE);
        }

        public void WriteToFileOnDisc(string text)
        {
            File.WriteAllText(SOME_FILE, text);
        }

        public string ReadFromIsolatedStorageFile()
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForAssembly();
            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(SOME_FILE, FileMode.OpenOrCreate, isoStore))
            {
                using (StreamReader reader = new StreamReader(isoStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public void WriteToIsolatedStorageFile(string text)
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForAssembly();
            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(SOME_FILE, FileMode.OpenOrCreate, FileAccess.Write, isoStore))
            {
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.Write(text);
                }
            }
        }

        public string ReadRegistryKey()
        {
            RegistryKey hklm = Registry.LocalMachine.OpenSubKey("HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0");
            return (string)hklm.GetValue("ProcessorNameString");
        }
    }
}
