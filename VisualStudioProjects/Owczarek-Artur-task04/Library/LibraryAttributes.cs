﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Permissions;

namespace Library
{
    public class LibraryAttributes
    {
        Library library = new Library();

        [PrincipalPermissionAttribute(SecurityAction.Demand, Role = "users")]
        public void WriteToFileOnDisc(string text)
        {
            library.WriteToFileOnDisc(text);
        }

        [PrincipalPermissionAttribute(SecurityAction.Demand, Role = "users")]
        public string ReadFileFromDisc()
        {
            return library.ReadFileFromDisc();
        }

        [PrincipalPermissionAttribute(SecurityAction.Demand, Authenticated = true)]
        public void WriteToIsolatedStorageFile(string text)
        {
            library.WriteToIsolatedStorageFile(text);
        }

        [PrincipalPermissionAttribute(SecurityAction.Demand, Authenticated = true)]
        public string ReadFromIsolatedStorageFile()
        {
            return library.ReadFromIsolatedStorageFile();
        }

        [PrincipalPermissionAttribute(SecurityAction.Demand, Role = "admins")]
        public string ReadRegistryKey()
        {
            return library.ReadRegistryKey();
        }
    }
}
